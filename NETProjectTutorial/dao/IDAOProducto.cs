﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.dao
{
    interface IDAOProducto: IDao<Producto>
    {

        Producto findById(int id);
        Producto findBySku(string sku);
        List<Producto> findByname(string name);

    }
}
